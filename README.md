# Projetos Mobile #

### ATGO ###
Aplicativo da Autoridade Tributária que simplifica a vida a quem emite recibos verdes, funcionando integrada com o Portal das Finanças e o E-Fatura. A aplicação permite efetuar operações e consultar os dados de atividade profissional registados, permitindo emitir ou anular faturas, faturas-recibo ou recibos, criar estes documentos pré-preenchidos para clientes frequentes e visualizar os montantes faturados, as despesas registadas e os valores acumulados para IRS, IVA e imposto de selo.  
**Xamarin Nativo**  
iOS - https://apps.apple.com/pt/app/atgo-gestão-de-atividade/id1601273900  
Android - https://play.google.com/store/apps/details?id=pt.gov.at.atgo

### BeTheNext ###
Plataforma criada para aproximar atletas de base, amadores ou profissionais à empresários, patrocinadores e/ou clubes esportivos.  
**Swift / Java**  
iOS - https://itunes.apple.com/us/app/bethenext/id1438791553?ls=1&mt=8  
Android - https://play.google.com/store/apps/details?id=br.com.bethenext

### Aprova Concursos ###
Aplicativo para estudantes de concursos públicos.  
**Objective-C / Java**  
iOS - https://itunes.apple.com/br/app/questoesde-concurso-aprova/id959916667?ls=1&mt=8  
Android - https://play.google.com/store/apps/details?id=br.com.hero99.iesde.aprovaconcursos

### Livro Digital ###
Aplicativo para estudantes da rede de ensino SAE, que transforma o conteúdo didático usado em sala de aula para livros interativos e objetos digitais dentro do celular. Também foi desenvolvida uma versão desktop para Windows, MacOS e Linux.  
**Swift / Java / Kotlin**  
iOS - https://itunes.apple.com/br/app/livros-digitais-sae-digital/id1057644222?mt=8  
Android - https://play.google.com/store/apps/details?id=com.iesde.livrodigital

### SAE Realidade Aumentada ###
O usuário tem acesso a diversas atividades virtuais de estudo por meio do celular ou tablet. São atividades interativas, jogos, vídeos, animações, textos e imagens diversas que ampliam o estudo do aluno através do livro. Ao direcionar a câmera do celular ou tablet para a página do livro, que tem o ícone de RA, a atividade aparece na tela.  
**Objective-C / Java** **Unity 3D**  
iOS - https://apps.apple.com/br/app/sae-ra/id1270184895?l=en  
Android - https://play.google.com/store/apps/details?id=com.sae.ra.livros

### Questões ENEM - SAE Digital ###
Aplicativo para estudantes que se preparam para o ENEM.  
**Objective-C / Java**  
iOS - https://itunes.apple.com/br/app/questoes-enem-sae-digital/id1027812828?mt=8  
Android - https://play.google.com/store/apps/details?id=br.com.iesde.saequestoes

### SAE Notifica ###
Aplicativo para que estudantes e responsáveis da rede de ensino SAE sejam informados dos status das atividades agendadas na Plataforma de Aprendizagem Adaptativa.  
**Swift / Java**  
iOS - https://itunes.apple.com/br/app/sae-notifica/id1135028846?l=en&mt=8  
Android - https://play.google.com/store/apps/details?id=br.com.iesde.appnotificacoes

### Coca-Cola Foto Felicidade ###
Aplicativo onde os usuários tiram fotos e selecionam filtros da Coca-Cola, agrupam até 4 fotos e compartilham nas redes sociais Facebook e Instagram.  
**Objective-C / Java**  
iOS - *removido da loja*  
Android - *removido da loja*

### Prepara Opet ###
Aplicativo para os estudantes da Opet se prepararem para o ENEM.  
**Objective-C / Java**  
iOS - *removido da loja*  
Android - *removido da loja*

### SlotoPrime ###
Jogo de slot para iOS, Android e Facebook.  
**Actionscript 3 / Java / PHP**  
iOS - https://itunes.apple.com/us/app/slotoprime-slot-machines/id608949867  
Android - *removido da loja*  
Facebook - *removido da loja*

### ClimbStreets ###
Aplicativo para escaladores. Construção do site integrado ao aplicativo: http://www.climbstreets.com/  
**Objective-C / Javascript / PHP**  
iOS - https://itunes.apple.com/br/app/climbstreets/id912785432?mt=8&ign-mpt=uo%3D4

### Fábulas de Gunter ###
Aplicativo que possibilita a leitura interativa da segunda coleção das fábulas de Gunter Pauli, onde o usuário pode conferir o conteúdo dos livros 15 ao 21 por meio da realidade aumentada.  
**Objective-C / Java / Unity 3D**  
iOS - https://itunes.apple.com/br/app/fabulas-gunter-volume-15-a-21/id1178054073?l=en&mt=8  
Android - *removido da loja*